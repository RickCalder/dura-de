<?php

    // Creating routes

    // Psr-7 Request and Response interfaces
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Dflydev\FigCookies\FigRequestCookies;
use Dflydev\FigCookies\FigResponseCookies;
use Dflydev\FigCookies\SetCookie;
use Mailgun\Mailgun;

define('WP_BASE', 'https://durathermfluids.com');

function generateDomains($lang) {
  $file = 'assets/dist/data/pages' . $lang . '.json';
  $pages = json_decode(file_get_contents($file));
  foreach( $pages as $page ){
    if($page->slug === 'microsites') {
      $current = $page;
      break;
    }
  }
  $links = $page->acf->microsite_site;
  // echo '<xmp>';print_r($links);die;
  return $links;
}
function getLanguage(Request $request, Response $response) {
  $path = $request->getUri()->getPath();
  $languages = array(
    'en',
    'de'
  );
  $lang = explode('/', $path);
  if(in_array($lang[1],$languages)) {
    return $lang[1];
  }
  return 'de';
}

// HOME ROUTE //

function generateDistributors( $distributors, $lang ) {
  $html = "";
  if( $lang === "en" ) {
    $telephone = "Telephone";
  } else if( $lang === "de" ){
    $telephone = "teléfono";
  }
  foreach( $distributors as $distributor ) {
    $html .= "<p>";
    $html .= $distributor->name . '<br>';
    $title = $distributor->title ? $distributor->title . '<br>' : '';
    $company = $distributor->company ? $distributor->company . '<br>' : '';
    $address = $distributor->address ? $distributor->address . '<br>' : '';
    $direct = $distributor->direct ? $distributor->direct . ' (direct)&nbsp;|&nbsp;' : '';
    $office = $distributor->office ? $distributor->office . ' (office) ' : '';
    $mobile = $distributor->mobile ? '<br><strong>Mobile:</strong> ' . $distributor->mobile : '';
    $html .= $title . $company . $address . '<strong> ' .$telephone. '</strong> ' .  $direct . $office .  $mobile;
    $html .= '<br><a href="mailto:' . $distributor->email . '">'.$distributor->email.'</a></p>';
  }
  return $html;
}

function altTags($lang) {
  // echo $lang; die;
  if($lang === 'de') {
    return [
      'logo'    => 'Markenlogo Duratherm Thermoöle',
      'oildrop' => 'Bild des Öltropfens, der für die lange Lebensdauer der Thermoöle von Duratherm steht.',
      'globe' => 'Bild der Weltkugel, die für das weltweite Händlernetz für Thermoöle von Duratherm steht.',
      'people'  => 'Bild mit drei Köpfen, die für die ausgeprägte Service-Verpflichtung von Duratherm stehen.',
      'hands' => 'Bild mit zwei Händen, die ein Blatt tragen und für die Umweltfreundlichkeit der Thermoöle von Duratherm stehen.',
      'magnify' => 'Bild einer Linse, die für die Tiefe unserer technischen Dokumentation steht.',
      'wrenches' => 'Bild mit gekreuztem Schraubenschlüssel und Schraubenzieher, die für unsere branchenspezifischen Rechner stehen.',
      'map_alt' => 'Graphische Darstellung der Weltkugel zur Unterstreichung der Regionen auf der ganzen Welt, in denen Vertreter von Duratherm ansässig sind.',
      'clientlogos' => 'Loghi di clienti che utilizzano i nostri fluidi termici e che rappresentano un ampio ventaglio di aziende leader globali che fanno affidamento a Duratherm',
      'graph' => 'Markenlogos der Thermoöl-Kunden, die für eine Vielzahl von weltweit führenden Unternehmen stehen, die Duratherm vertrauen'
    ];
  } else {
    return [
      'logo'    => 'Duratherm Thermal Fluids brand logo',
      'oildrop' => 'Icon of oil drop representing the long life of Duratherm thermal fluid.',
      'globe' => 'Icon of a globe representing Duratherm\'s global thermal fluid distribution network.',
      'people'  => 'Icon of three heads signifying Duratherm\'s strong service commitment.',
      'hands' => 'Icon of two hands supporting a leaf representing Duratherm’s eco-friendly thermal fluids. ',
      'magnify' => 'Icon of magnifying lens representing our in-depth technical papers.',
      'wrenches' => 'Icon of crossed wrench and screwdriver representing our industry specific calculators.',
      'map_alt'=> 'Graphic representation of the globe highlighting regions throughout the globe where Duratherm has representatives.',
      'clientlogos' => 'Thermal fluid customer brand logos representing a wide range of leading global industries that rely on Duratherm',
      'graph' => 'Bar graph comparing IP48 test results of 9 industry leading thermal fluids, showing Duratherm\'s superior, lower levels of oxidation and minimal change in viscosity.'
    ];
  }
}

$app->get('/', function (Request $request, Response $response, $args) {

  $lang = getLanguage( $request, $response );
  $file = 'assets/dist/data/common_de.json';
  $common = json_decode(file_get_contents($file));
  $products1 = json_decode(file_get_contents('assets/dist/data/products_de.json'));
  $products = [];
  foreach($products1 as $product) {
    if($product->slug == 'duratherm-xlt-50' || $product->slug == 'duratherm-xlt-120' || $product->slug == 'duratherm-g') {
      continue;
    } else {
      array_push($products, $product);
    }
  };
  $distributors = json_decode(file_get_contents('assets/dist/data/distributors_de.json'));
  $html = generateDistributors($distributors, "de");
  $pages = json_decode(file_get_contents('assets/dist/data/pages_de.json'));
  $testimonial_array = json_decode(file_get_contents('assets/dist/data/testimonials_de.json'));
  shuffle($testimonial_array);
  $testimonials = array();
  $i=0;
  foreach ($testimonial_array as $key => $value) {
    $testimonials[$i]['author'] = $value->author;
    $testimonials[$i]['content'] = $value->content;
    $i++;
  }

  $currentPage = [];
  foreach ($pages as $page ) {
    if( $page->slug === 'homepage' ) {
      $currentPage = $page;
    }
    if ($page->slug === 'downloads') {
      $downloads = $page->acf;
    }
  }
// echo '<xmp>';print_r($pages);die;
  foreach( $currentPage->acf->tech_papers[0] as $key => $value ) {
    if( $key =='tech_fluid_degradation' ) {
      $degradation = $value;
    } elseif ( $key =='tech_fluid_maintenance' ) {
      $maintenance = $value;
    } else {
      $selection = $value;
    }
  }
  foreach( $currentPage->acf->tech_tools[0] as $key => $value ) {
    if( $key =='tech_fluid_comparison' ) {
      $comparison = $value;
    } else {
      $calculators = $value;
    }
  }
  $vars = [
    'page' => [
    'title' => 'Thermöle von Duratherm: langlebig und ungiftig',
    'description' => 'Unerreichter Service mit hohem Flammpunkt, lebensmittelverträglich und kostengünstige Alternativen mit nachgewiesener längerer Haltbarkeit als Thermoöle des Wettbewerbs.',
    'common'   =>$common,
    'acf'   => $currentPage->acf,
    'products' => $products,
    'downloads' => $downloads,
    'testimonials' => $testimonials,
    'download_base' => WP_BASE,
    'degradation' => $degradation,
    'maintenance' => $maintenance,
    'distributors' => $html,
    'selection' => $selection,
    'comparison' => $comparison,
    'calculators' => $calculators,
    'site_languages' => ['de','en'],
    'lang' => '',
    'navPage' => 'home-nav',
    'alt_tags' => altTags('de'),
    'footer_links' => generateDomains('_de'),
    'alternates' => [
      [
        'link' => 'https://xn--durathermle-zfb.de/',
        'lang' => 'de-de',
        'external' => '1'
      ],
      [
        'link' => 'https://xn--durathermle-zfb.de/en',
        'lang' => 'en-de',
        'external' => '1'
      ],
      [
        'link' => 'https://durathermfluids.com',
        'lang' => 'en-us',
        'external' => '1'
      ]
    ]
    ],
  ];  
  // echo '<xmp>';print_r($vars);die;
  return $this->view->render($response, 'home.twig', $vars);

})->setName('home');

$app->get('/datenschutz-richtlinie', function (Request $request, Response $response, $args) {
  $file = 'assets/dist/data/common_de.json';
  $common = json_decode(file_get_contents($file));
  $products1 = json_decode(file_get_contents('assets/dist/data/products_de.json'));
  $products = [];
  foreach($products1 as $product) {
    if($product->slug == 'duratherm-xlt-50' || $product->slug == 'duratherm-xlt-120' || $product->slug == 'duratherm-g') {
      continue;
    } else {
      array_push($products, $product);
    }
  };
  // print_r($products);die;
  $distributors = json_decode(file_get_contents('assets/dist/data/distributors_de.json'));
  $html = generateDistributors($distributors, "de");

  $vars = [
    'base_url' => $request->getUri()->getBasePath(),
    'page' => [
    'title' => 'Datenschutz-Richtlinie :: Thermöle von Duratherm',
    'description' => 'Thermöle von Duratherm',
    'common'   =>$common,
    'products' => $products,
    'download_base' => WP_BASE,
    'site_languages' => ['de','en'],
    'footer_links' => generateDomains('_de'),
    'alternates' => [
      [
        'link' => 'https://xn--durathermle-zfb.de/privacy',
        'lang' => 'de-de',
        'external' => '1'
      ],
      [
        'link' => 'https://durathermfluids.com/privacy/',
        'lang' => 'en-us',
        'external' => '1'
      ]
    ]
    ],
  ];  
  // echo '<xmp>';print_r($vars);die;
  return $this->view->render($response, 'privacy.twig', $vars);

})->setName('intimite');

$app->get('/en', function (Request $request, Response $response, $args) {
  $file = 'assets/dist/data/common_en.json';
  $common = json_decode(file_get_contents($file));
  $pages = json_decode(file_get_contents('assets/dist/data/pages.json'));
  $products1 = json_decode(file_get_contents('assets/dist/data/products.json'));
  $products = [];
  foreach($products1 as $product) {
    if($product->slug == 'duratherm-xlt-50' || $product->slug == 'duratherm-xlt-120' || $product->slug == 'duratherm-g') {
      continue;
    } else {
      array_push($products, $product);
    }
  };
  // print_r($products);die;
  $distributors = json_decode(file_get_contents('assets/dist/data/distributors.json'));
  $html = generateDistributors($distributors, "en");
  $testimonial_array = json_decode(file_get_contents('assets/dist/data/testimonials.json'));
  shuffle($testimonial_array);
  $testimonials = array();
  $i=0;
  foreach ($testimonial_array as $key => $value) {
    $testimonials[$i]['author'] = $value->author;
    $testimonials[$i]['content'] = $value->content;
    $i++;
  }

  $currentPage = [];
  foreach ($pages as $page ) {
    if( $page->slug === 'homepage' ) {
      $currentPage = $page;
    }
    if ($page->slug === 'downloads') {
      $downloads = $page->acf;
    }
  }
// echo '<xmp>';print_r($pages);die;
  foreach( $currentPage->acf->tech_papers[0] as $key => $value ) {
    if( $key =='tech_fluid_degradation' ) {
      $degradation = $value;
    } elseif ( $key =='tech_fluid_maintenance' ) {
      $maintenance = $value;
    } else {
      $selection = $value;
    }
  }
  foreach( $currentPage->acf->tech_tools[0] as $key => $value ) {
    if( $key =='tech_fluid_comparison' ) {
      $comparison = $value;
    } else {
      $calculators = $value;
    }
  }
  $vars = [
    'base_url' => $request->getUri()->getBasePath(),
    'page' => [
    'title' => 'Thermal Fluids by Duratherm: Long-Lasting, Non-Toxic',
    'description' => 'Unparalleled service with high flash point, food grade, and economical options proven to last longer than competing thermal fluids.',
    'common'   =>$common,
    'acf'   => $currentPage->acf,
    'products' => $products,
    'downloads' => $downloads,
    'testimonials' => $testimonials,
    'download_base' => WP_BASE,
    'degradation' => $degradation,
    'maintenance' => $maintenance,
    'distributors' => $html,
    'selection' => $selection,
    'site_languages' => ['de','en'],
    'comparison' => $comparison,
    'calculators' => $calculators,
    'alt_tags' => altTags('en'),
    'lang' => 'en',
    'footer_links' => generateDomains(''),
    'alternates' => [
      [
        'link' => 'https://xn--durathermle-zfb.de/',
        'lang' => 'de-de',
        'external' => '1'
      ],
      [
        'link' => 'https://xn--durathermle-zfb.de/en',
        'lang' => 'en-de',
        'external' => '1'
      ],
      [
        'link' => 'https://durathermfluids.com',
        'lang' => 'en-us',
        'external' => '1'
      ]
    ]
    ],
  ];  
  // echo '<xmp>';print_r($vars);die;
  return $this->view->render($response, 'home.twig', $vars);

})->setName('home2');
    
// RESOURCES ROUTE //

$app->get('/ressourcen', function (Request $request, Response $response, $args) {
  $distributors = json_decode(file_get_contents('assets/dist/data/distributors.json'));
  $html = generateDistributors($distributors, "de");
  $lang = getLanguage( $request, $response );
  $file = 'assets/dist/data/common_de.json';
  $common = json_decode(file_get_contents($file));
  $products1 = json_decode(file_get_contents('assets/dist/data/products_de.json'));
  $products = [];
  foreach($products1 as $product) {
    if($product->slug == 'duratherm-xlt-50' || $product->slug == 'duratherm-xlt-120' || $product->slug == 'duratherm-g') {
      continue;
    } else {
      array_push($products, $product);
    }
  };
  // echo '<xmp>';print_r($products);die;
  $comp_products = json_decode(file_get_contents('assets/dist/data/comp_products.json'));
  $pages = json_decode(file_get_contents('assets/dist/data/pages_de.json'));
  $currentPage = [];
  $downloads = [];
  foreach ($pages as $page ) {
    if( $page->slug === 'resources' ) {
      $currentPage = $page;
    }
    if ($page->slug === 'downloads') {
      $downloads = $page->acf;
    }
  }
  $currentPage->acf->res_tech = str_replace('report-degradation',  $downloads->fluid_degradation, $currentPage->acf->res_tech);
  $currentPage->acf->res_tech = str_replace('report-maintenance',  $downloads->fluid_maintenance, $currentPage->acf->res_tech);
  $currentPage->acf->res_tech = str_replace('report-selection',  $downloads->fluid_selection, $currentPage->acf->res_tech);
  $vars = [
    'base_url' => $request->getUri()->getBasePath(),
    'page' => [
    'title' => 'Thermoöl-Vegleichstool, Rechner und Tipps',
    'description' => 'Vergleichen Sie die auf dem Markt verfügbaren Thermoöle und nutzen Sie unsere Rechner, um hilfreiche Tipps zur Auswahl des richtigen Thermoöls für Ihre Anwendung zu erhalten.',
    'common'   =>$common,
    'products' => $products,
    'comp_products' => $comp_products,
    'acf'   => $currentPage->acf,
    'downloads' =>$downloads,
    'resources_page' => '/ressourcen',
    'navPage' => 'resources-nav',
    'download_base' => WP_BASE,
    'pdf_report_img' => 'assets/dist/img/pdf_report_en1-sm.png',
    'site_languages' => ['de','en'],
    'distributors' => $html,
    'lang' => '',
    'alt_tags' => altTags('de'),
    'footer_links' => generateDomains('_de'),
    'alternates' => [
      [
        'link' => 'https://xn--durathermle-zfb.de/ressourcen',
        'lang' => 'de-de',
        'external' => '1'
      ],
      [
        'link' => 'https://xn--durathermle-zfb.de/en/resources',
        'lang' => 'en-de',
        'external' => '1'
      ],
      [
        'link' => 'https://durathermfluids.com/resources/',
        'lang' => 'en-us',
        'external' => '1'
      ]
    ]
    ],
  ];  

  return $this->view->render($response, 'resources.twig', $vars);

})->setName('ressourcen');
    

$app->get('/en/resources', function (Request $request, Response $response, $args) {
  $lang = getLanguage( $request, $response );
  $distributors = json_decode(file_get_contents('assets/dist/data/distributors.json'));
  $html = generateDistributors($distributors, "en");
  $file = 'assets/dist/data/common_en.json';
  $products1 = json_decode(file_get_contents('assets/dist/data/products.json'));
  $products = [];
  foreach($products1 as $product) {
    if($product->slug == 'duratherm-xlt-50' || $product->slug == 'duratherm-xlt-120' || $product->slug == 'duratherm-g') {
      continue;
    } else {
      array_push($products, $product);
    }
  };
  $comp_products = json_decode(file_get_contents('assets/dist/data/comp_products.json'));
  $common = json_decode(file_get_contents($file));
  $pages = json_decode(file_get_contents('assets/dist/data/pages.json'));
  $currentPage = [];
  $downloads = [];
  foreach ($pages as $page ) {
    if( $page->slug === 'resources' ) {
      $currentPage = $page;
    }
    if ($page->slug === 'downloads') {
      $downloads = $page->acf;
    }
  }
  $degradation = $downloads->fluid_degradation_de_en;
  $maintenance = $downloads->fluid_maintenance_de_en;
  $selection = $downloads->fluid_selection_be_dn;
  $currentPage->acf->res_tech = str_replace('report-degradation',  $degradation, $currentPage->acf->res_tech);
  $currentPage->acf->res_tech = str_replace('report-maintenance',  $maintenance, $currentPage->acf->res_tech);
  $currentPage->acf->res_tech = str_replace('report-selection',  $selection, $currentPage->acf->res_tech);
  $vars = [
    'base_url' => $request->getUri()->getBasePath(),
    'page' => [
    'title' => 'Thermal Fluid Comparison Tool, Calculators, and Tips',
    'description' => 'Compare fluids from across the industry, use our calculators, and get helpful tips on choosing the right thermal fluid for your application.',
    'common'   =>$common,
    'products' => $products,
    'acf'   => $currentPage->acf,
    'downloads' =>$downloads,
    'comp_products' => $comp_products,
    'site_languages' => ['de','en'],
    'resources_page' => '/resources',
    'navPage' => 'ressourcen-nav',
    'content' => $currentPage->content,
    'download_base' => WP_BASE,
    'pdf_report_img' => 'assets/dist/img/pdf_report_en1-sm.png',
    'distributors' => $html,
    'alt_tags' => altTags('en'),
    'footer_links' => generateDomains(''),
    'alternates' => [
      [
        'link' => 'https://xn--durathermle-zfb.de/ressourcen',
        'lang' => 'de-de',
        'external' => '1'
      ],
      [
        'link' => 'https://xn--durathermle-zfb.de/en/resources',
        'lang' => 'en-de',
        'external' => '1'
      ],
      [
        'link' => 'https://durathermfluids.com/resources/',
        'lang' => 'en-us',
        'external' => '1'
      ]
    ]
    ],
  ];  
  return $this->view->render($response, 'resources.twig', $vars);

})->setName('resources');
// PRODUCTS ROUTE //

$app->get('/produkte', function (Request $request, Response $response, $args)   {
  $distributors = json_decode(file_get_contents('assets/dist/data/distributors.json'));
  $html = generateDistributors($distributors, "de");
  $lang = getLanguage( $request, $response );
  $file = 'assets/dist/data/common_'. $lang . '.json';
  $common = json_decode(file_get_contents($file));
  $products1 = json_decode(file_get_contents('assets/dist/data/products_de.json'));
  $products = [];
  foreach($products1 as $product) {
    if($product->slug == 'duratherm-xlt-50' || $product->slug == 'duratherm-xlt-120' || $product->slug == 'duratherm-g') {
      continue;
    } else {
      array_push($products, $product);
    }
  };
  $pages = json_decode(file_get_contents('assets/dist/data/pages_de.json'));

  // echo '<xmp>';print_r($testimonials); die;
  $currentPage = [];
  foreach ($pages as $page ) {
    if( $page->slug === 'products' ) {
      $currentPage = $page;
    }
  }
  
  $vars = [
    'base_url' => $request->getUri()->getBasePath(),
    'page' => [
    'title' => 'Thermoöl-Auswahltool | Duratherm',
    'description' => 'Unsere benutzerfreundlichen Tools helfen Ihnen dabei, das richtige Thermoöl auszusuchen, oder nehmen Sie die Beratung von unserem technischen Ingenieur-Support in Anspruch.',
    'common'   =>$common,
    'products' => $products,
    'content' => $currentPage->content,
    'download_base' => WP_BASE,
    'products_page' => '/produkte',
    'resources_page' => '/ressourcen',
    'distributors' => $html,
    'site_languages' => ['de','en'],
    'navPage' => 'products-nav',
    'alt_tags' => altTags('de'),
    'footer_links' => generateDomains('_de'),
    'alternates' => [
      [
        'link' => 'https://xn--durathermle-zfb.de/produkte',
        'lang' => 'de-de',
        'external' => '1'
      ],
      [
        'link' => 'https://xn--durathermle-zfb.de/en/products',
        'lang' => 'en-de',
        'external' => '1'
      ],
      [
        'link' => 'https://durathermfluids.com/products/',
        'lang' => 'en-us',
        'external' => '1'
      ]
    ]
    ],
  ];  
// echo '<xmp>';print_r($vars); die;
  return $this->view->render($response, 'products.twig', $vars);

})->setName('produkte');

$app->get('/produkte/{slug}', function (Request $request, Response $response, $args)   {
  $distributors = json_decode(file_get_contents('assets/dist/data/distributors.json'));
  $html = generateDistributors($distributors, "de");
  $file = 'assets/dist/data/common_de.json';
  $common = json_decode(file_get_contents($file));
  $products1 = json_decode(file_get_contents('assets/dist/data/products_de.json'));
  $products = [];
  foreach($products1 as $product) {
    if($product->slug == 'duratherm-xlt-50' || $product->slug == 'duratherm-xlt-120' || $product->slug == 'duratherm-g') {
      continue;
    } else {
      array_push($products, $product);
    }
  };
  $testimonial_array = json_decode(file_get_contents('assets/dist/data/testimonials_de.json'));
  shuffle($testimonial_array);
  $testimonials = array();
  $i=0;
  foreach ($testimonial_array as $key => $value) {
    $testimonials[$i]['author'] = $value->author;
    $testimonials[$i]['content'] = $value->content;
    $i++;
  }
  // echo '<xmp>';print_r($products); die;
  $currentProduct = [];
  foreach ($products as $product ) {
    if( $product->slug === $args['slug'] ) {
      $currentProduct = $product;
    }
  }
  
  $vars = [
    'base_url' => $request->getUri()->getBasePath(),
    'page' => [
    'title' => $currentProduct->seo_title,
    'description' => $currentProduct->seo_description,
    'image_alt_tag' => $currentProduct->image_alt_tag,
    'common'   =>$common,
    'product' => $currentProduct,
    'products' => $products,
    'testimonials' => $testimonials,
    'temperature_range' => explode(",", $currentProduct->temperature_range),
    'viscosity_range' => explode(",", $currentProduct->viscosity_range),
    'distributors' => $html,
    'download_base' => WP_BASE,
    'site_languages' => ['de','en'],
    'lang' => 'de',
    'lang2' => 'german',
    'alt_tags' => altTags('en'),
    'footer_links' => generateDomains(''),
    'alternates' => [
      [
        'link' => 'https://xn--durathermle-zfb.de/produkte/' . $currentProduct->slug,
        'lang' => 'de-de',
        'external' => '1'
      ],
      [
        'link' => 'https://xn--durathermle-zfb.de/en/products/' . $currentProduct->slug,
        'lang' => 'en-de',
        'external' => '1'
      ],
      [
        'link' => 'https://durathermfluids.com/heat-transfer-fluid/' . $currentProduct->slug,
        'lang' => 'en-us',
        'external' => '1'
      ]
    ]

    ],
  ];  
// echo '<xmp>';print_r($vars); die;
  return $this->view->render($response, 'product-single.twig', $vars);

})->setName('products');

$app->get('/en/products', function (Request $request, Response $response, $args)   {
  $lang = getLanguage( $request, $response );
  $file = 'assets/dist/data/common_'. $lang . '.json';
  $distributors = json_decode(file_get_contents('assets/dist/data/distributors.json'));
  $html = generateDistributors($distributors, "en");
  $common = json_decode(file_get_contents($file));
  $products1 = json_decode(file_get_contents('assets/dist/data/products.json'));
  $products = [];
  foreach($products1 as $product) {
    if($product->slug == 'duratherm-xlt-50' || $product->slug == 'duratherm-xlt-120' || $product->slug == 'duratherm-g') {
      continue;
    } else {
      array_push($products, $product);
    }
  };
  $pages = json_decode(file_get_contents('assets/dist/data/pages.json'));
  $currentPage = [];
  foreach ($pages as $page ) {
    if( $page->slug === 'products' ) {
      $currentPage = $page;
    }
  }
  $vars = [
    'base_url' => $request->getUri()->getBasePath(),
    'page' => [
    'title' => $currentPage->title . '  :: Duratherm Fluids',
    'description' => 'Duratherm Fluids',
    'common'   =>$common,
    'products' => $products,
    'content' => $currentPage->content,
    'site_languages' => ['de','en'],
    'download_base' => WP_BASE,
    'products_page' => '/en/products',
    'resources_page' => '/en/resources',
    'distributors' => $html,
    'navPage' => 'products-nav',
    'alt_tags' => altTags('en'),
    'footer_links' => generateDomains(''),
    'alternates' => [
      [
        'link' => 'https://xn--durathermle-zfb.de/produkte',
        'lang' => 'de-de',
        'external' => '1'
      ],
      [
        'link' => 'https://xn--durathermle-zfb.de/en/products',
        'lang' => 'en-de',
        'external' => '1'
      ],
      [
        'link' => 'https://durathermfluids.com/products/',
        'lang' => 'en-us',
        'external' => '1'
      ]
    ]
    ],
  ];  
// echo '<xmp>';print_r($vars); die;
  return $this->view->render($response, 'products.twig', $vars);

})->setName('products2');


$app->get('/en/products/{slug}', function (Request $request, Response $response, $args)   {
  $distributors = json_decode(file_get_contents('assets/dist/data/distributors.json'));
  $html = generateDistributors($distributors,"en");
  $file = 'assets/dist/data/common_en.json';
  $common = json_decode(file_get_contents($file));
  $products1 = json_decode(file_get_contents('assets/dist/data/products.json'));
  $products = [];
  foreach($products1 as $product) {
    if($product->slug == 'duratherm-xlt-50' || $product->slug == 'duratherm-xlt-120' || $product->slug == 'duratherm-g') {
      continue;
    } else {
      array_push($products, $product);
    }
  };
  $testimonial_array = json_decode(file_get_contents('assets/dist/data/testimonials.json'));
  shuffle($testimonial_array);
  $testimonials = array();
  $i=0;
  foreach ($testimonial_array as $key => $value) {
    $testimonials[$i]['author'] = $value->author;
    $testimonials[$i]['content'] = $value->content;
    $i++;
  }
  // echo '<xmp>';print_r($products); die;
  $currentProduct = [];
  foreach ($products as $product ) {
    if( $product->slug === $args['slug'] ) {
      $currentProduct = $product;
    }
  }
  
  $vars = [
    'base_url' => $request->getUri()->getBasePath(),
    'page' => [
    'title' => $currentProduct->seo_title,
    'description' => $currentProduct->seo_description,
    'image_alt_tag' => $currentProduct->image_alt_tag,
    'common'   =>$common,
    'product' => $currentProduct,
    'products' => $products,
    'testimonials' => $testimonials,
    'temperature_range' => explode(",", $currentProduct->temperature_range),
    'viscosity_range' => explode(",", $currentProduct->viscosity_range),
    'distributors' => $html,
    'download_base' => WP_BASE,
    'lang' => 'en',
    'lang2' => 'english',
    'navPage' => 'products-nav',
    'alt_tags' => altTags('en'),
    'site_languages' => ['de','en'],
    'footer_links' => generateDomains(''),
    'alternates' => [
      [
        'link' => 'https://xn--durathermle-zfb.de/produkte/' . $currentProduct->slug,
        'lang' => 'de-de',
        'external' => '1'
      ],
      [
        'link' => 'https://xn--durathermle-zfb.de/en/products/' . $currentProduct->slug,
        'lang' => 'en-de',
        'external' => '1'
      ],
      [
        'link' => 'https://durathermfluids.com/heat-transfer-fluid/' . $currentProduct->slug,
        'lang' => 'en-us',
        'external' => '1'
      ]
    ]

    ],
  ];  
// echo '<xmp>';print_r($vars); die;
  return $this->view->render($response, 'product-single.twig', $vars);

})->setName('products');
$app->get('/update_products', function (Request $request, Response $response, $args) {

  $api_url = WP_BASE;
  
  $languages = array (
    '',
    'de'
  );
  foreach( $languages as $language ) {
    $client = new GuzzleHttp\Client();
    $data = $client->request('GET', $api_url . '/' . $language . '/wp-json/wp/v2/products?per_page=50')->getBody()->getContents();
    
    $products = json_decode($data);
    $new_products = array();
    $comp_products = array();
    $new_products[]['fluid_data']= array();
    $comp_products[]['fluid_data']= array();
    // echo '<xmp>';print_r($products);
    $i = 0;
    $y = 0;
    foreach( $products as $product ) {
      if(  $product->acf->duratherm_product !== "") {
        if( $product->slug === 'duratherm-g') {
          continue;
        }
        $new_products[$i]['id'] = $product->id;
        $new_products[$i]['slug'] = $product->slug;
        $new_products[$i]['seo_title'] = $product->acf->seo_title;
        $new_products[$i]['seo_description'] = $product->acf->seo_description;
        $new_products[$i]['image_alt_tag'] = $product->acf->image_alt_tag;
        $new_products[$i]['name'] = $product->title->rendered;
        $new_products[$i]['cooling_fluid'] = isset($product->acf->cooling_fluid[0]) ? 'yes' : 'no';
        $new_products[$i]['description'] = $product->content->rendered;
        $new_products[$i]['excerpt'] = $product->excerpt->rendered;
        $new_products[$i]['max_temp'] = ceil(($product->acf->max_bulk_temp - 32) * (5/9));
        $new_products[$i]['min_temp'] = ceil(($product->acf->optimal_minimum_temp - 32) * (5/9));
        $new_products[$i]['max_temp_standard'] = $product->acf->max_bulk_temp;
        $new_products[$i]['min_temp_standard'] = $product->acf->minimum_pump_temp;
        $new_products[$i]['film_temp'] = ceil(($product->acf->max_film_temp - 32) * (5/9));
        $new_products[$i]['flashpoint'] = ceil(($product->acf->flash_point - 32) * (5/9));
        $new_products[$i]['food_grade'] = isset($product->acf->food_grade[0]) ? 'yes' : 'no';
        $new_products[$i]['quick_facts'] = $product->acf->quick_facts;
        $new_products[$i]['duratherm_product'] = $product->acf->duratherm_product[0];
        $new_products[$i]['chemical_composition'] = $product->acf->chemical_composition;
        $new_products[$i]['pour_point'] = ceil(($product->acf->pour_point - 32) * (5/9));
        $new_products[$i]['fire_point'] = ceil(($product->acf->fire_point - 32) * (5/9));
        $new_products[$i]['auto_ignition_temp'] = $product->acf->auto_ignition_temp !="" ? ceil(($product->acf->auto_ignition_temp - 32) * (5/9)) : "N/A";
        $new_products[$i]['specific_gravity'] = $product->acf->specific_gravity != "" ? $product->acf->specific_gravity : "N/A";
        $new_products[$i]['average_molecular_weight'] = $product->acf->average_molecular_weight !="" ? $product->acf->average_molecular_weight : "N/A";
        $new_products[$i]['minimum_pump_temp'] = $product->acf->minimum_pump_temp;
        $new_products[$i]['optimal_minimum_temp'] = $product->acf->optimal_minimum_temp;
        $new_products[$i]['thermal_expansion_coefficient'] = $product->acf->thermal_expansion_coefficient !="" ? $product->acf->thermal_expansion_coefficient : "N/A";
        $new_products[$i]['distillation_range_10'] = ceil(($product->acf->distillation_range_10 - 32) * (5/9));
        $new_products[$i]['distillation_range_90'] = ceil(($product->acf->distillation_range_90 - 32) * (5/9));
        $new_products[$i]['temperature_range'] = $product->acf->temperature_range;
        $new_products[$i]['viscosity_range'] = $product->acf->viscosity_range;
        $new_products[$i]['tds'] = $product->acf->tds;
        $new_products[$i]['tds_de_en'] = $product->acf->tds_de_en;
        $new_products[$i]['sds'] = $product->acf->sds;
        $new_products[$i]['viscosity_104'] = $product->acf->viscosity_104;
        // $new_products[$i]['fluid_data'] = $product->acf->fluid_data;
        $x=0;
        $fluidData = file_get_contents($product->acf->fluid_data);
        file_put_contents('assets/dist/data/' .$product->slug . '.csv', $fluidData);
        $newFluidData = array_map('str_getcsv', file('assets/dist/data/' .$product->slug . '.csv'));
        foreach($newFluidData as $item) {
          $new_products[$i]['fluid_data'][$x]['temperature'] = ceil(($item[0] - 32) * (5/9));
          $new_products[$i]['fluid_data'][$x]['density'] = round($item[1] * 0.016018463, 3);
          $new_products[$i]['fluid_data'][$x]['dynamic_viscosity'] = round($item[3], 2);
          $new_products[$i]['fluid_data'][$x]['viscosity'] = round($item[3], 2);
          $new_products[$i]['fluid_data'][$x]['kinematic_viscosity'] = round($item[2], 2);
          $new_products[$i]['fluid_data'][$x]['thermal_conductivity'] = round($item[4] * 01.73, 3);
          $new_products[$i]['fluid_data'][$x]['heat_capacity'] = round($item[5] * 4.1868, 3);
          $new_products[$i]['fluid_data'][$x]['vapour_pressure'] = round($item[6] * 6.89476, 3);
          $new_products[$i]['fluid_data_standard'][$x]['temperature'] = $item[0];
          $new_products[$i]['fluid_data_standard'][$x]['density'] = $item[1];
          $new_products[$i]['fluid_data_standard'][$x]['dynamic_viscosity'] =$item[3];
          $new_products[$i]['fluid_data_standard'][$x]['viscosity'] = $item[3];
          $new_products[$i]['fluid_data_standard'][$x]['kinematic_viscosity'] = $item[2];
          $new_products[$i]['fluid_data_standard'][$x]['thermal_conductivity'] = $item[4];
          $new_products[$i]['fluid_data_standard'][$x]['heat_capacity'] = $item[5];
          $new_products[$i]['fluid_data_standard'][$x]['vapour_pressure'] = $item[6];
          $x++;
        }
        $i++;
      } else {
        $comp_products[$y]['id'] = $product->id;
        $comp_products[$y]['name'] = $product->title->rendered;
        $comp_products[$y]['cooling_fluid'] = isset($product->acf->cooling_fluid[0]) ? 'yes' : 'no';
        $comp_products[$y]['max_temp'] = ceil(($product->acf->max_bulk_temp - 32) * (5/9));
        $comp_products[$y]['min_temp'] = ceil(($product->acf->optimal_minimum_temp - 32) * (5/9));
        $comp_products[$y]['max_temp_standard'] = $product->acf->max_bulk_temp;
        $comp_products[$y]['min_temp_standard'] = $product->acf->minimum_pump_temp;
        $comp_products[$y]['film_temp'] = "N/A";
        $comp_products[$y]['flashpoint'] = ceil(($product->acf->flash_point - 32) * (5/9));
        $comp_products[$y]['food_grade'] = isset($product->acf->food_grade[0]) ? 'yes' : 'no';
        $comp_products[$y]['duratherm_product'] = "no";
        $comp_products[$y]['chemical_composition'] = $product->acf->chemical_composition;
        $comp_products[$y]['pour_point'] = ceil(($product->acf->pour_point - 32) * (5/9));
        $comp_products[$y]['fire_point'] = $product->acf->fire_point !="" ? ceil(($product->acf->fire_point - 32) * (5/9)) : "N/A";
        $comp_products[$y]['auto_ignition_temp'] = $product->acf->auto_ignition_temp !="" ? ceil(($product->acf->auto_ignition_temp - 32) * (5/9)) : "N/A";
        $comp_products[$y]['specific_gravity'] = $product->acf->specific_gravity != "" ? $product->acf->specific_gravity : "N/A";
        $comp_products[$y]['average_molecular_weight'] = $product->acf->average_molecular_weight !="" ? $product->acf->average_molecular_weight : "N/A";
        $comp_products[$y]['minimum_pump_temp'] = $product->acf->minimum_pump_temp;
        $comp_products[$y]['optimal_minimum_temp'] = $product->acf->optimal_minimum_temp;
        $comp_products[$y]['thermal_expansion_coefficient'] = $product->acf->thermal_expansion_coefficient !="" ? $product->acf->thermal_expansion_coefficient : "N/A";
        $comp_products[$y]['distillation_range_10'] = ceil(($product->acf->distillation_range_10 - 32) * (5/9));
        $comp_products[$y]['distillation_range_90'] = ceil(($product->acf->distillation_range_90 - 32) * (5/9));
        $comp_products[$y]['temperature_range'] = $product->acf->temperature_range;
        $comp_products[$y]['viscosity_range'] = $product->acf->viscosity_range;
        $comp_products[$y]['viscosity_104'] = $product->acf->viscosity_104;
        $x=0;
        $fluidData = file_get_contents($product->acf->fluid_data);
        file_put_contents('assets/dist/data/' .$product->slug . '.csv', $fluidData);
        $newFluidData2 = array_map('str_getcsv', file('assets/dist/data/' .$product->slug . '.csv'));
        foreach($newFluidData2 as $item) {
          $comp_products[$y]['fluid_data'][$x]['temperature'] = ceil(((int)$item[0] - 32) * (5/9));
          $comp_products[$y]['fluid_data'][$x]['density'] = $item[1] != "" ? round($item[1] * 0.016018463, 3) : "N/A";
          $comp_products[$y]['fluid_data'][$x]['dynamic_viscosity'] = $item[3] != "" ? round($item[3], 2) : "N/A";
          $comp_products[$y]['fluid_data'][$x]['viscosity'] = $item[3] != "" ? round($item[3], 2) : "N/A";
          $comp_products[$y]['fluid_data'][$x]['kinematic_viscosity'] = $item[2] != "" ? round($item[2], 2) : "N/A";
          $comp_products[$y]['fluid_data'][$x]['thermal_conductivity'] = $item[4] != "" ? round($item[4] * 01.73, 3) : "N/A";
          $comp_products[$y]['fluid_data'][$x]['heat_capacity'] = $item[5] != "" ? round($item[5] * 4.1868, 3) : "N/A";
          $comp_products[$y]['fluid_data'][$x]['vapour_pressure'] = $item[6] != "" ? round($item[6] * 6.89476, 3) : "N/A";
          $comp_products[$y]['fluid_data_standard'][$x]['temperature'] = $item[0];
          $comp_products[$y]['fluid_data_standard'][$x]['density'] = $item[1];
          $comp_products[$y]['fluid_data_standard'][$x]['dynamic_viscosity'] =$item[3];
          $comp_products[$y]['fluid_data_standard'][$x]['viscosity'] = $item[3];
          $comp_products[$y]['fluid_data_standard'][$x]['kinematic_viscosity'] = $item[2];
          $comp_products[$y]['fluid_data_standard'][$x]['thermal_conductivity'] = $item[4];
          $comp_products[$y]['fluid_data_standard'][$x]['heat_capacity'] = $item[5];
          $comp_products[$y]['fluid_data_standard'][$x]['vapour_pressure'] = $item[6];
          $x++;
        }
        $y++;
      }
    }

    $tempProducts = array();
    foreach ($new_products as $key => $row) {
        $tempProducts[$key] = $row['max_temp'];
    }
    array_multisort($tempProducts, SORT_DESC, $new_products);

    if( !empty($comp_products) && isset($comp_products[0]['id'])) {
      $tempCompProducts = array();
      foreach ($comp_products as $key => $row) {
          $tempCompProducts[$key] = $row['name'];
      }
      array_multisort($tempCompProducts, SORT_ASC, $comp_products);
    }

    //Save the WP content to a data.json file.
    $lang = $language !== '' ? '_' . str_replace('/', '', $language) : '';
    file_put_contents('assets/dist/data/products' .$lang . '.json', json_encode($new_products));
    file_put_contents('assets/dist/data/comp_products' .$lang . '.json', json_encode($comp_products));
  }
});

$app->get('/update_pages', function (Request $request, Response $response, $args) {
  
  $api_url = WP_BASE;

  
  $languages = array (
    '',
    'de'
  );
  foreach( $languages as $language ) {
    $client = new GuzzleHttp\Client();
    $data = $client->request('GET', $api_url . '/'  . $language . '/wp-json/wp/v2/pages?per_page=100')->getBody()->getContents();
    echo $api_url . '/'  . $language . '/wp-json/wp/v2/pages';
    $pages = json_decode($data);
    
    $new_pages = array();
    $i = 0;
    foreach( $pages as $page ) {
      $new_pages[$i]['id'] = $page->id;
      $new_pages[$i]['slug'] = $page->slug;
      $new_pages[$i]['title'] = $page->title->rendered;
      $new_pages[$i]['content'] = $page->content->rendered;
      $new_pages[$i]['acf'] = $page->acf;
      $i++;
    }
    
    //Save the WP content to a data.json file.
    // echo '<xmp>';print_r($new_pages);die;
    $lang = $language !== '' ? '_' . str_replace('/', '', $language) : '';
    file_put_contents('assets/dist/data/pages' .$lang . '.json', json_encode($new_pages));
  }
});

$app->get('/update_testimonials', function (Request $request, Response $response, $args) {
  
  $api_url = WP_BASE;
  
  $languages = array (
    '',
    'de'
  );
  foreach( $languages as $language ) {
    $client = new GuzzleHttp\Client();
    $data = $client->request('GET', $api_url . '/'  . $language . '/wp-json/wp/v2/testimonials?per_page=50')->getBody()->getContents();
    
    $testimonials = json_decode($data);
    // echo '<xmp>';print_r($testimonials);die;
    
    $new_testimonials = array();
    $i = 0;
    foreach( $testimonials as $testimonial ) {
      $new_testimonials[$i]['id'] = $testimonial->id;
      $new_testimonials[$i]['content'] = $testimonial->content->rendered;
      $new_testimonials[$i]['author'] = $testimonial->acf->testimonial_author;
      $i++;
    }
    
    //Save the WP content to a data.json file.
    // echo '<xmp>';print_r($new_testimonials);die;
    $lang = $language !== '' ? '_' . str_replace('/', '', $language) : '';
    file_put_contents('assets/dist/data/testimonials' .$lang . '.json', json_encode($new_testimonials));
  }
});

$app->get('/update_distributors', function (Request $request, Response $response, $args) {
  
  $api_url = WP_BASE;
  
  $languages = array (
    '',
    'de'
  );
  foreach( $languages as $language ) {
    $client = new GuzzleHttp\Client();
    $data = $client->request('GET', $api_url . '/'  . $language . '/wp-json/wp/v2/distributors?per_page=50&order=asc')->getBody()->getContents();
    
    $distributors = json_decode($data);
    // echo '<xmp>'.$language;print_r($distributors);
    
    $new_distributors = array();
    $i = 0;
    foreach( $distributors as $distributor ) {
      if( $distributor->acf->distributor_site === 'DE') {
        $new_distributors[$i]['name'] = $distributor->title->rendered;
        $new_distributors[$i]['title'] = $distributor->acf->distributor_title;
        $new_distributors[$i]['company'] = $distributor->acf->distributor_company;
        $new_distributors[$i]['address'] = $distributor->acf->distributor_address;
        $new_distributors[$i]['direct'] = $distributor->acf->distributor_phone[0]->direct;
        $new_distributors[$i]['office'] = $distributor->acf->distributor_phone[0]->office;
        $new_distributors[$i]['mobile'] = $distributor->acf->distributor_phone[0]->mobile;
        $new_distributors[$i]['email'] = $distributor->acf->distributor_email;
      }
      $i++;
    }

    //Save the WP content to a data.json file.
    $lang = $language !== '' ? '_' . str_replace('/', '', $language) : '';
    file_put_contents('assets/dist/data/distributors' .$lang . '.json', json_encode($new_distributors));
  }
});

if( !function_exists('ceiling') )
{
    function ceiling($number, $significance = 1)
    {
        return ( is_numeric($number) && is_numeric($significance) ) ? (ceil($number/$significance)*$significance) : false;
    }
}

// Send Emails

$app->post('/sendmail', function (Request $request, Response $response, $args) {
  // print json_encode( array('status'=> 'okay'));
  // return true;

  $mg = new Mailgun('key-aad8e3d4108aab8793e3bebd3a6dc3ed');
  $post = $request->getParsedBody();
  sendConfirmation($post);
  $distributors = json_decode(file_get_contents('assets/dist/data/distributors.json'));
  $recipients = '';
  $length = count((array)$distributors);
  $i = 1;
  foreach($distributors as $dist) {
    if($i == $length) {
      $recipients .= $dist->email;
    } else {
      $recipients .= $dist->email . ', ';
    }
    $i++;
  }
  $products = '';
  $i = 0;
  foreach( $post["quote-products"] as $product ) {
    $products .= $product . " Container Size:  " . $post["quote-container-size"][$i] . " Quantity:  " . $post["quote-quantity"][$i] . "\r\n\r\n";
    $i++;
  }

  $text = "About You Section\r\n\r\n". "First Name: " . $post["first_name"] . "\r\nLast Name: " . $post["last_name"] . "\r\n" . 
    "Company: " . $post["company"] . "\r\n" . 
    "Phone: " . $post["phone"] . "\r\n" .
    "Email: " . $post["email"] . "\r\n" .
    "Country: " . $post["country"] . "\r\n" .
    "General Comments: " . nl2br($post["comments"]) ."\r\n\r\n";

  $text .= "About Application Section\r\n\r\n" .
    "Temperatures (C): " . $post["temperature"] . "\r\n" .
    "Volume (litres): " .  $post["volume"] . "\r\n" .
    "Application: " .  nl2br($post["application"]) . "\r\n\r\n";
  if(isset($post["quote-products"][0])) {
    $text .= "Quote Section\r\n\r\n" . 
      $products;
    }
// echo $text;die;
  $response = $mg->sendMessage('mg.durathermole.de', [
    // 'from'    => 'inquiry@mg.xn--durathermle-zfb.de',
    'from' => 'inquiry@mg.durathermole.de',
    'sender' => 'inquiry@mg.durathermole.de',
    'to'      =>'inquiry@durathermfluids.com',
    // 'to'        => 'calder12@gmail.com',
    //'cc'      => $recipients, 
    //'bcc'     => 'd.derksen@caldic.nl ',
    'subject' => 'Duratherm DE Website Request', 
    'text'    => $text
  ]);

  if( $response->http_response_code == '200' ) {
    print json_encode( array('status'=> 'sent'));
  } else {
    print json_encode( array('status'=> 'error'));
  }
});

function sendConfirmation($post) {
  $mg = new Mailgun('key-aad8e3d4108aab8793e3bebd3a6dc3ed');
  $distributors = json_decode(file_get_contents('assets/dist/data/distributors.json'));
  $translations = json_decode(file_get_contents('assets/dist/data/common_'.$post['site-lang'].'.json'));
  

  $text = '<p><img src="https://durathermole.de/assets/dist/img/duratherm.png" width="200"/></p>';
  $text .= str_replace('distributor-details', generateDistributors($distributors, $post['site-lang']), $translations->contact_confirm);

  $response = $mg->sendMessage('mg.durathermole.de', [
  // 'from'    => 'inquiry@mg.xn--durathermle-zfb.de', 
  // 'sender'  => 'inquiry@mg.xn--durathermle-zfb.de',
  'from' => 'inquiry@mg.durathermole.de',
  'sender' => 'inquiry@mg.durathermole.de',
  'h:Reply-To' => 'inquiry@durathermfluids.com',
  'to'      => $post['email'],
  'subject' => $translations->contact_subject, 
  'html'    => $text
]);
}