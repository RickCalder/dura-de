"use strict"

$(document).ready(function() {  
  
  if(typeof productsPage == 'undefined') return;
  if( location.pathname !== productsPage) return;
  $(".compare-body").html("");
  var compareProducts = [];
   if( localStorage.getItem("compareProducts") !== null ){
    compareProducts = JSON.parse(localStorage.getItem("compareProducts"));
    populateCompare(compareProducts);
    if( compareProducts.length > 0 ) {
      $(".compare-submit").addClass("open").removeClass("closed");
      $(".compare-submit").fadeIn();
    }
  }
  var compareIds = [];
  for( var i = 0; i < compareProducts.length; i++ ){
    compareIds.push(compareProducts[i].id);
  }
  $(".card-container").each( function(){
    var id = $(this).data("product")
    if( $.inArray(id, compareIds) > -1 ) {
      // $(this).find(".product-card").css({"border":"2px solid green"});
      $(this).find("#compare-" + id).prop("checked", "checked");
    }
  });

  $("#compare-submit").on("click", function() {
    var selections = [];
    var i = 0;
    $(".compare-checkbox").each(function() {
      if($(this).is(":checked")) {
        selections[i] = $(this).val();
        i++;
      }
    });
  })


  $(document).on("click", ".compare-checkbox", function(){

    var id = $(this).val();

    if($(this).is(":checked")) {
      if( compareProducts.length == 4 ){
        alert("You can compare a maximum of 4 products");
        $(this).attr('checked', false);
        return;
      }
      // $(this).closest(".product-card").css({"border":"2px solid green"});
      var result = $.grep(fullProductsArray, function(e){ return e.id == id; });
      var productAdd = {};
      productAdd['id'] = result[0].id;
      productAdd['name'] = result[0].name;
      productAdd['slug'] = result[0].slug;
      compareProducts.push(productAdd);
      populateCompare(compareProducts);
    } else {
      // $(this).closest(".product-card").css({"border":"2px solid #DDD"})
      var id = $(this).val();
      compareProducts = $.grep(compareProducts, function(v) {
        return v.id != id;
      });
      populateCompare(compareProducts);
    }

    if(compareProducts.length != 0 ) {
      if( $(".compare-submit").hasClass("compressed") ) {
        $(".compare-body").fadeIn();
        $(".compare-toggle i").removeClass("fa-chevron-up").addClass("fa-chevron-down")
      } else if(!$(".compare-submit").hasClass("open") ){
        $(".compare-submit").addClass("open").removeClass("closed");
        $(".compare-submit").fadeIn();
        $(".compare-toggle i").removeClass("fa-chevron-up").addClass("fa-chevron-down")
      }
    } else {
      localStorage.removeItem("compareProducts");
      $(".compare-submit").fadeOut();
      $(".compare-submit").addClass("closed").removeClass("open");
    }
  });

  $(document).on("click", ".compare-remove", function(e) {
    e.preventDefault();
    var id = $(this).data("id");
    compareProducts = $.grep(compareProducts, function(v) {
      return v.id != id;
    });
    if(compareProducts.length == 0 ) {
      $(".compare-submit").fadeOut();
    }
    $(".compare-checkbox").each(function(){
      if($(this).val() == id) {
        $(this).attr('checked', false);
        // $(this).closest(".product-card").css({"border":"2px solid #DDD"})
      }
    });
    if( compareProducts.length == 0 ){
      localStorage.removeItem("compareProducts");
      $(".compare-body").html("");
      $(".compare-submit").fadeOut().removeClass("open").removeClass("compressed");
    }
    populateCompare(compareProducts);
  })

  $(".compare-toggle").on("click", function(e) {
    e.preventDefault();
    $(".compare-body").slideToggle();
    if( $(this).find("i").hasClass("fa-chevron-down") ) {
      $(this).find("i").removeClass("fa-chevron-down").addClass("fa-chevron-up");
      $(".compare-submit").removeClass("open").removeClass("closed").addClass("compressed");
      $(".compare-submit").addClass("closed").removeClass("open");
    } else {
      $(this).find("i").addClass("fa-chevron-down").removeClass("fa-chevron-up");
      $(".compare-submit").removeClass("closed").removeClass("compressed").addClass("open");
    }
  });

  $(".compare-cancel").on("click", function(e) {
    e.preventDefault();
    compareProducts = [];
    populateCompare(compareProducts);
    $(".compare-checkbox").each(function(){
      $(this).attr('checked', false);
      // $(this).closest(".product-card").css({"border":"2px solid #DDD"});
    });
    $(".compare-submit").fadeOut().removeClass("open").removeClass("compressed");
  })

  $(document).on("click", ".go-compare", function(e) {
    e.preventDefault();
    location.href = resourcesPage + "#compare-section";
  })

})
function populateCompare(products) {
  // console.log(resourcesPage)
  $(".compare-body").html("");
  var html = "<ul>";
  for( var i = 0; i < products.length; i++ ) {
    html += "<li>" + products[i].name + "<a class='compare-remove' href='' data-id='" + products[i].id + "'><i class='fa fa-times-circle' aria-hidden='true'></i></a></li>";
  }
  html += "</ul>";
  if( products.length > 1 ){
    html += "<div class='col-xs-12 right'><a href='#' class='btn go-compare'>" + common_terms.compare + "</a></div>";
  } else if (products.length === 0) {
  } else {
    html += "<div class='col-xs-12 right'><a href='" + resourcesPage + "#compare-section' class='btn go-compare disabled'>" + common_terms.compare + "</a></div>";
  }
  localStorage.setItem("compareProducts", JSON.stringify(products));
  $(".compare-body").html(html);
}