$('#OVForm').validate({
  rules: {
    'viscosity1OV': {required: true, number: true},
    'viscosity2OV': {required: true, number: true},
    'temp1OV': {required: true, number: true},
    'temp2OV': {required: true, number: true},
    'operational-tempOV': {required: true, number: true}
	},
    errorClass: "error",
    errorElement: "span",
    errorPlacement: function(error, element) {
		error.addClass('help-inline').insertAfter(element);
	},
    submitHandler: function(form) {
      operatingViscosity();
		}
});