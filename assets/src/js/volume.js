$('#VolumeForm1').validate({
	    rules: {
		    'diameterVC': {required: true, number: true},
		    'lengthVC': {required: true, number: true}
		},
        errorClass: "error",
        errorElement: "span",
        errorPlacement: function(error, element) {
				error.addClass('help-inline').insertAfter(element);
			},
	        submitHandler: function(form) {
		        volumeCylinder();
			}
    });
    
	$('#VolumeForm2').validate({
	    rules: {
		    'length1RP': {required: true, number: true},
		    'length2RP': {required: true, number: true},
		    'length3RP': {required: true, number: true}
		},
        errorClass: "error",
        errorElement: "span",
        errorPlacement: function(error, element) {
				error.addClass('help-inline').insertAfter(element);
			},
	        submitHandler: function(form) {
		        volumeRectangularPrism();
			}
    });

	$('#VolumeForm3').validate({
	    rules: {
		    'diameterVS': {required: true, number: true}
		},
        errorClass: "error",
        errorElement: "span",
        errorPlacement: function(error, element) {
				error.addClass('help-inline').insertAfter(element);
			},
	        submitHandler: function(form) {
		        volumeSphere();
			}
});