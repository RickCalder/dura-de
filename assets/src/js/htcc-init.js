$(document).ready(function() {
	var loc = window.location.href.substring(window.location.href.lastIndexOf('/')).replace(/#.*$/,'');

  if(typeof resourcesPage == 'undefined') return;
	if( loc == resourcesPage ) {
		$.getScript("/assets/dist/js/htcc.js", function(){
			var options = {};
			options.products = fullProductsArrayHtcc;
			options.el = "htcc";
	    options.metric = true;
	    options.selectedProductId = -1;
			var htcc = new CoefficientCalculator(options);
			$.getScript("/assets/dist/js/encoding-indexes.js", function() {
				$.getScript("/assets/dist/js/encoding.js", function() {
					$.getScript("/assets/dist/js/fileSaver.js");
				});
			});

		  $('#frmHTCC').validate({
		    rules: {
			    'temperature': {required: true, number: true},
			    'pipe-diameter': {required: true, number: true},
			    'fluid-velocity-min': {required: true, number: true},
			    'fluid-velocity-max': {required: true, number: true, greaterThan: "#fluid-velocity-min-htcc"},
			    'fluid-velocity-inc': {required: true, number: true},
			    'fluid-viscosity': {required: true, number: true},
			    'fluid-specific-heat': {required: true, number: true},
			    'fluid-conductivity': {required: true, number: true},
			    'fluid-density': {required: true, number: true}
				},
		        errorClass: "error",
		        errorElement: "span",
		        errorPlacement: function(error, element) {
							error.addClass('help-inline').insertAfter(element);
						},
		        submitHandler: function(form) {
			        htcc.generateOutput();
				}
		  });

		});
	}

  $.validator.addMethod('greaterThan', function(value, element, param) {
	  return ( parseInt(value) > parseInt(jQuery(param).val()) );
	}, 'Must be greater than min' );
})