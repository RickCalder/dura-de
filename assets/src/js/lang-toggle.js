"use strict";

$(document).ready(function(){

  $(".lang-selected").on("click", function(e) {
    e.preventDefault()
    $(".lang-list").slideToggle("fast")
  })
  $(".lang-selector").on("click", function(e) {
    e.preventDefault()
    var html = $(this).html() + '<span class="glyphicon glyphicon-chevron-down down-arrow"></span>';
    $(".lang-selected").html(html)
    $(".lang-list").slideToggle("fast")
  })

  if(location.href.indexOf("/en") > -1) {
    $(".fr").removeClass("active");
    $(".en").addClass("active");
  } else {
    $(".fr").addClass("active");
    $(".en").removeClass("active");
  }

  $(".lang-selector").on("click", function(e) {
    e.preventDefault();
    localStorage.removeItem("filterSettings");
    localStorage.removeItem("compareProducts");
    var selectorLanguage = $(this).data('language').toLowerCase()
    console.log(selectorLanguage)
    if( selectorLanguage == commonTerms.lang ) return;

    var loc = window.location.href;

    if(loc.indexOf("produkte") > -1) {
      var newUri = loc.replace("produkte", "en/products");
      changePage(newUri);
    } else if (loc.indexOf("products") > -1) {
      var newUri = loc.replace("en/products", "produkte");
      changePage(newUri);
    } else if(loc.indexOf("ressourcen") > -1) {
      var newUri = loc.replace("ressourcen", "en/resources");
      changePage(newUri);
    } else if (loc.indexOf("resources") > -1) {
      var newUri = loc.replace("en/resources", "ressourcen");
      changePage(newUri);
    } else if (loc.indexOf("datenschutz-richtlinie") > -1) {
      var newUri = loc.replace("/datenschutz-richtlinie", "/en");
      changePage(newUri);
    } else if(loc.indexOf("/en") > -1) {
      var newUri = loc.replace("/en", "");
      changePage(newUri);
    } else {
      var newUri = loc + "en";
      changePage(newUri);
    }
  });

  function changePage(newUri) {
    localStorage.setItem("languageChange", "true");
    document.location.href = newUri;
  }
});