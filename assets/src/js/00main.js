"use strict";

(function($) {

  $("#" + navPage).addClass("active");

  $('[data-toggle="popover"]').popover()
  $("body").on("click", function(e) {
    var target = $(e.target);
    if(! target.is($('.fa-question-circle'))) {
      $('[data-toggle="popover"]').popover("hide");
    }
  })
  var equalheight = function(container){
    if(window.width < 768) return;
    var currentTallest = 0,
    currentRowStart = 0,
    rowDivs = new Array(),
    $el,
    topPosition = 0;
    $(container).each(function() {
      $el = $(this);
      $($el).height("auto")
      topPosition = $el.position().top;

      if (currentRowStart != topPosition) {
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
          rowDivs[currentDiv].height(currentTallest);
        }
        rowDivs.length = 0; // empty the array
        currentRowStart = topPosition;
        currentTallest = $el.height();
        rowDivs.push($el);
      } else {
        rowDivs.push($el);
        currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
      }
      for (var currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
        rowDivs[currentDiv].height(currentTallest);
      }
   });
  }

  var throttle = function(func, wait) {
    var context, args, timeout, throttling, more, result;
    var whenDone = _.debounce(function(){ more = throttling = false; }, wait);
    return function() {
      context = this; args = arguments;
      var later = function() {
        timeout = null;
        if (more) func.apply(context, args);
        whenDone();
      };
      if (!timeout) timeout = setTimeout(later, wait);
      if (throttling) {
        more = true;
      } else {
        result = func.apply(context, args);
      }
      whenDone();
      throttling = true;
      return result;
    };
  };

  var topButton = function() {
    var windowTop = $(window).scrollTop();
    if( windowTop > 200 ) {
      $(".back-to-top").slideDown();
      $(".header-container").addClass("full-color")
    } else {
      $(".back-to-top").slideUp();
      $(".header-container").removeClass("full-color")
    }
  }

  var compareScroll = function() {
    $(".comparison-wrapper").scroll(function() {
      var compareTop = $(this).scrollTop();
      $(".compare-nav-container").css("top", compareTop);
      $(".compare-back").css("top", compareTop);
    })
  }
  $(".scroll-link").on("click", function(e) {
    e.preventDefault();
    var $target = "#" + $(this).data("target");
    $("html, body").animate({
      scrollTop: $($target).offset().top
    }, 500);
  });

  $(".back-to-top").on("click", function() {
    $("html, body").animate({
      scrollTop: 0
    }, 500);
  });

  $(window).load(equalheight(".tools1"));
  $(window).load(equalheight(".tools2"));
  $(window).load(equalheight(".res-tech-paper"));
  $(window).load(equalheight(".tip-container"));
  $(window).load(equalheight(".tool-container"));
  $(window).scroll(throttle( topButton, 50));
  $(window).scroll(throttle( compareScroll, 50));

  var template = $(".product-row").first().clone();
  var formCount = 0;

  $("#add-product").on("click", function() {
    var removeMe = "<div class='col-xs-1'><a href='#' class='remove-me'><i class='fa fa-minus-circle' aria-hidden='true'></i></a></div>";
    formCount++;
    var lastGroup = $(".product-row").last();

    var section = template.clone().find(':input').each(function(){
      var newId = this.id + formCount;
      $(this).prev().attr('for', newId);
      this.id = newId;
    }).end()
    .insertAfter(lastGroup).append(removeMe).hide().slideDown(300);
    return false;
  });

  //testimonials rotator
  if(typeof testimonials != 'undefined') {
    var interval = testimonials[0].content.length * 40;
    $("#testimonial").html(testimonials[0].content);
    $("#testimonial-author").html(testimonials[0].author);
    var tCounter = 1;

    function callback() {
      interval = (testimonials[tCounter].content.length * 40)
      $("#testimonial").html(testimonials[tCounter].content);
      $("#testimonial-author").html(testimonials[tCounter].author);
      if(tCounter < testimonials.length - 1) {
        tCounter++;
      } else {
        tCounter = 0;
      }
      setTimeout( callback, interval );
    }
    setTimeout(callback, interval);
  };

  if( $(".client-logos").length !==0 ) {
   var scroller = $('#scroller div.innerScrollArea');
    var scrollerContent = scroller.children('ul');
    scrollerContent.children().clone().appendTo(scrollerContent);
    var curX = 0;
    scrollerContent.children().each(function(){
        var $this = $(this);
        $this.css('left', curX);
        curX += $this.outerWidth(true);
    });
    var fullW = curX / 2;
    var viewportW = scroller.width();

    // Scrolling speed management
    var controller = {curSpeed:0, fullSpeed:1};
    var $controller = $(controller);
    var tweenToNewSpeed = function(newSpeed, duration)
    {
        if (duration === undefined)
            duration = 600;
        $controller.stop(true).animate({curSpeed:newSpeed}, duration);
    };


    // Scrolling management; start the automatical scrolling
    var doScroll = function()
    {
        var curX = scroller.scrollLeft();
        var newX = curX + controller.curSpeed;
        if (newX > fullW*2 - viewportW)
            newX -= fullW;
        scroller.scrollLeft(newX);
    };
    setInterval(doScroll, 10);
    tweenToNewSpeed(controller.fullSpeed);
  }
})(jQuery);



//Iubenda code
var _iub = _iub || [];
_iub.csConfiguration = {
    cookiePolicyId: 7879863,
    siteId: 577905,
    lang: "en",
    callback: {
        onConsentGiven: function(){
          dataLayer.push({'event': 'iubenda_consent_given'});
        }
      }

    };