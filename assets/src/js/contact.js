$(".get-quote").on("click", function(e) {
	e.preventDefault();
	var modal = "#modal-" + $(this).data("product");
		if($(modal).data("bs.modal") && $(modal).data("bs.modal").isShown) {
			$(modal).modal("hide");
		}
  var $target = "#" + $(this).data("target");
  $("html, body").animate({
    scrollTop: $($target).offset().top
  }, 500);
  var selector = "#quote-products option[value=" + $(this).data("product") +"]";
  $(selector).prop("selected", true);
})

$('#contact-form').validate({
  rules: {
    'first_name': {required: true},
    'last_name': {required: true},
    'email': {required: true, email: true},
    'country': {required: true},
  },
    errorClass: "error",
    errorElement: "span",
    errorPlacement: function(error, element) {
    error.addClass('help-inline').insertAfter(element);
  },
    submitHandler: function(form) {
      $("#contact-submit").prop('disabled', 'disabled')
      $('.loader').slideDown()
      if(location.host != 'dura-fr.dev' && location.protocol != 'https') {
        baseUrl = baseUrl.replace('http', 'https');
      }
      var analyticsArray = {};
      analyticsArray["event"] = "contactForm";
      analyticsArray['formType'] = $("#quote-products").val() != 'Product Type' ? "rfq" : "general";
      var url = location.origin + "/sendmail"; 
      $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: $("#contact-form").serialize(), 
        success: function(data) {
          if(data.status !== "error") {
            var successMessage = emailSuccess.replace("email-address", $("#email").val())
            successMessage = successMessage.replace("distributor-details", distributors)
            $('.loader').slideUp()
            $(".email-response")
              .html(successMessage)
              .addClass("success")
              .slideDown();
            dataLayer.push(analyticsArray)
          } else {
            
          }
        }
      });
      return false;
    }
});

$(document).on("click", ".remove-me", function(e) {
  e.preventDefault();
  $(this).closest(".product-row").slideUp().remove();
})