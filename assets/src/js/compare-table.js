"use strict"

$(document).ready(function() {
  var loc = window.location.href.substring(window.location.href.lastIndexOf('/'));

  if(typeof resourcesPage == "undefined") return;
  if( loc.replace(/#.*$/,'') != resourcesPage ) return;  

  if( localStorage.getItem("compareProducts") !== null ){
    var productsArray = JSON.parse(localStorage.getItem("compareProducts"));
  } else {
    var productsArray = {};
  }
  // Initialize Products
  var products = fullProductsArray;

  for( var i=0; i< products.length; i++ ) {
    if( products[i].name === "Duratherm 630" ) {
      var baseProd = products[i].id;
    }
  }
  if( loc.indexOf("compare-section") === -1 ) {
    for( var i=0; i < compProducts.length; i++ ) {
      products.push( compProducts[i]);
    }
  }
  var isMetric = true;

  var compareIds = [];
  for( var i =0; i< productsArray.length; i++ ) {
    compareIds.push(productsArray[i].id);
  }

  if( compareIds.length < 2 ) {
    compareIds = [];
    compareIds.push(baseProd);
  }

  $(".print-compare").on("click", function(e) {
    printTables();
  });

  $(".export-compare").on("click", function(e) {
    e.preventDefault();
    $(printTable).tableToCSV();
  });

  init(compareIds);
  var printTable;

  function init(compareIds) {
    var productsToCompare = [];
    var emptyProduct = {};
    for( var i=0; i < products.length; i++ ) {
      if($.inArray(products[i].id, compareIds) > -1) {
        var arrayPosition = $.inArray(products[i].id, compareIds);
        productsToCompare.splice(arrayPosition, 0, products[i] );
      }
    }

    $.when( getTempRanges(productsToCompare) ).then( function(data) {
      var temperatureRange = data['reg'];
      var viscosityRange = data['vis']
      $.when(resolveProductProperties(productsToCompare)).then( function(data2){
        renderTables(data2, temperatureRange, viscosityRange, compareIds);
        printTable = renderPrintTables(data2, temperatureRange, viscosityRange, compareIds);
      })
    })
  }

  function printTables() {
    var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0,titlebar=0');
    WinPrint.document.write("<title>Duratherm Fluids Compare Products</title>");
    var css = '<style>';
        css += "body { font-family: sans-serif; -webkit-print-color-adjust: exact; }";
        css += '.page-header { display: none; }';
        css += 'table { width: 100%; border-spacing: 0; border-collapse: collapse; font-size: 0.8em;}';
        css += "td { padding: 10px 5px; }";
        css += "th { padding: 10px 5px; text-align: left;}";
        css += "tr { border-bottom:1px solid #ccc;}";
        css += ".grey-row { background: #f6f6f6 }";
        css += ".main-header { background-color: #bbb; color: #fff; font-weight: bold}";
        css += ".secondary-header { background-color: #ddd; font-weight: bold}";
        css += '</style>';
    WinPrint.document.write(css);
    WinPrint.document.write(printTable);
    WinPrint.document.close();
    WinPrint.focus();
    WinPrint.print();
    WinPrint.close(); 
  }
  var html = "<div class='product-list' data-product='0' data-name='Select Fluid'>" + common_terms.select_fluid + "</div>";
  var html2 = "<div class='product-list' data-product='0' data-name='Select Fluid'>" + common_terms.select_fluid + "</div>";

  for( var i = 0; i < products.length; i++ ) {
    html += "<div class='product-list' data-product='" + products[i].id + "' data-name='" + products[i].name + "'>" + products[i].name + "</div>"
  }
  for( var i = 0; i < products.length; i++ ) {
    if( products[i].duratherm_product === "yes" ) {
      html2 += "<div class='product-list' data-product='" + products[i].id + "' data-name='" + products[i].name + "'>" + products[i].name + "</div>"
    }
  }
  $(".all-fluids").html(html);
  $(".fluids1").html(html2);
  

  // Dropdown List Animations
  $(".compare-header").on("click", function(){
    var thisOne = $(this).closest('.compare-column').find('.fluids');
    var visible = $(this).closest('.compare-column').find('.fluids').css("display") == "block" ? "yes" : "no";

    $(".fluids").hide();
    if( visible == "no" ) {
      thisOne.slideDown();
      $(".compare-nav").css("z-index", "6001");
    }

    checkForFluids();
  });

  $(document).on("click", ".product-list", function(){
    $(".fluids").hide();
    checkForFluids();

    var header = $(this).closest(".fluids").data("header");
    $("#" + header).data("product", $(this).data("product"));
    $(this).closest(".prod-header").data("product", $(this).data("product"));
    var newProducts = [];
    $(".prod-header").each(function(index, elem) {
      newProducts.push($(elem).data("product"));
    })
    init(newProducts)
  })
});

// Start functions


function checkForFluids() {
  var fluidsVisible = 0;    

  $(".fluids").each(function(){
    if($(this).css("display") == "block") {
      fluidsVisible++;
    }
  })
  if( fluidsVisible > 0 ) 
    $(".compare-nav").css("height", "600px");        
   else {
    $(".compare-nav").css("height", "60px");
  }   
}


function renderPrintTables(products, temperatureRange, viscosityRange, compareIds) {

  var html = "<table class='print-compare-table'><tr><th>" + common_terms.products + "</th>";

  for( var i=0; i<products.length; i++) {
    html += "<th>" + products[i].name + "</th>";
  }
  html += "<tr class='main-header'><td colspan='" + (products.length + 1) + "'><strong>" + common_terms.temperature_ratings + "</strong></td>";
  html += "</tr><tr><td>" + common_terms.max_bulk_temp + "</td>";

  for( var i=0; i<products.length; i++) {
    var max_temp = passTemp(products[i].max_temp);
    html += "<td>" + max_temp + "</td>";
  }
  html += "</tr><tr class='grey-row'><td>" + common_terms.max_film_temp + "</td>";

  for( var i=0; i<products.length; i++) {
    var film_temp = passTemp(products[i].film_temp);
    html += "<td>" + film_temp + "</td>";
  }
  html += "</tr><tr><td>" + common_terms.pour_point + "</td>";

  for( var i=0; i<products.length; i++) {
    var pour_point = passTemp(products[i].pour_point)
    html += "<td>" + pour_point + "</td>";
  }
  html += "<tr class='main-header'><td colspan='" + (products.length + 1) + "'><strong>Safety Data</strong></td>";
  html += "</tr><tr><td>" + common_terms.flashpoint + "</td>";

  for( var i=0; i<products.length; i++) {
    var flashpoint = passTemp(products[i].flashpoint)
    html += "<td>" + flashpoint + "</td>";
  }
  html += "</tr><tr class='grey-row'><td>" + common_terms.fire_point + "</td>";

  for( var i=0; i<products.length; i++) {
    var fire_point = passTemp(products[i].fire_point)
    html += "<td>" + fire_point + "</td>";
  }
  html += "</tr><tr><td>" + common_terms.auto_ignition + "</td>";

  for( var i=0; i<products.length; i++) {
    var auto_ignition_temp = passTemp(products[i].auto_ignition_temp)
    html += "<td>" + auto_ignition_temp + "</td>";
  }
  html += "<tr class='main-header'><td colspan='" + (products.length + 1) + "'><strong>" + common_terms.thermal_properties + "</strong></td>";
  html += "</tr><tr><td><strong>" + common_terms.thermal_expansion_coefficient + "</strong></td>";

  for( var i=0; i<products.length; i++) {
    var thermal_expansion_coefficient = passTec(products[i].thermal_expansion_coefficient);
    html += "<td>" + thermal_expansion_coefficient + "</td>";
  }
  html += "<tr class='secondary-header'><td colspan='" + (products.length + 1) + "'><strong>" + common_terms.thermal_conductivity + " W/m·k</strong></td></tr>";
  var tc = generatePrintTables( "thermal_conductivity", products, temperatureRange );
  html += tc;

  html += "<tr class='secondary-header'><td colspan='" + (products.length + 1) + "'><strong>" + common_terms.heat_capacity + " (kJ/kg·K)</strong></td></tr>";
  var hc = generatePrintTables( "heat_capacity", products, temperatureRange );
  html += hc;

  html += "<tr class='main-header'><td colspan='" + (products.length + 1) + "'><strong>" + common_terms.physical_properties + "</strong></td>";
  html += "</tr><tr><td><strong>" + common_terms.chemical_composition + "</strong></td>";

  for( var i=0; i<products.length; i++) {
    var chemical_composition = products[i].chemical_composition;
    html += "<td>" + chemical_composition + "</td>";
  }

  html += "<tr class='secondary-header'><td colspan='" + (products.length + 1) + "'><strong>" + common_terms.kinematic_viscosity + " (cSt)</strong></td></tr>";
  var kv = generatePrintTables( "kinematic_viscosity", products, viscosityRange );
  html += kv;

  html += "<tr class='secondary-header'><td colspan='" + (products.length + 1) + "'><strong>" + common_terms.density + " (kg/L)</strong></td></tr>";
  var den = generatePrintTables( "density", products, temperatureRange );
  html += den;

  html += "<tr class='secondary-header'><td colspan='" + (products.length + 1) + "'><strong>" + common_terms.vapour_pressure + " (kPa)</strong></td></tr>";
  var vp = generatePrintTables( "vapour_pressure", products, temperatureRange );
  html += vp;
  html += "<tr class='secondary-header'><td colspan='" + (products.length + 1) + "'><strong>" + common_terms.distillation_range + "</strong></td>";
  html += "</tr><tr><td>10%</td>";

  for( var i=0; i<products.length; i++) {
    var distillation_range_10 = passTemp(products[i].distillation_range_10);
    html += "<td>" + distillation_range_10 + "</td>";
  }
  html += "</tr><tr><td>90%</td>";

  for( var i=0; i<products.length; i++) {
    var distillation_range_90 = passTemp(products[i].distillation_range_90);
    html += "<td>" + distillation_range_90 + "</td>";
  }
  html += "</tr><tr><td><strong>" + common_terms.average_molecular_weight + "</strong></td>";

  for( var i=0; i<products.length; i++) {
    var average_molecular_weight = products[i].average_molecular_weight;
    html += "<td>" + average_molecular_weight + "</td>";
  }
  html += "</tr><tr><td><strong>" + common_terms.specific_gravity + " 16&deg;C</strong></td>";

  for( var i=0; i<products.length; i++) {
    var specific_gravity = products[i].specific_gravity != "N/A" ? roundNumber(products[i].specific_gravity, 3) : "N/A";
    html += "<td>" + specific_gravity + "</td>";
  }

  html +="</table>";
  return html;
}

function generatePrintTables( param, products, temperatureRange ) {
  var html = "";

  for( var i=0; i<temperatureRange.length; i++ ) {
    var rowClass = i % 2 ? "grey-row": "";
    html += "<tr class='" + rowClass + "'>";
    html += "<td>" + passTemp(temperatureRange[i]) + "</td>";
    for( var j=0; j<=3; j++ ) {
      if( ( _.isEmpty(products[j]))  ) {
      } else {
        var currentTemp = parseInt(temperatureRange[i]);
        for( var x=0; x< products[j].fluid_data.length; x++ ){
          var productTemp = parseInt(products[j].fluid_data[x].temperature);
          if( currentTemp == productTemp ){
            var temp = "<td>" + roundNumber(products[j].fluid_data[x][param], 3) +"</td>";
            break;
          } else if( products[j].duratherm_product !== "yes") {
            var temp = "<td>N/A</td>";
          } else {
            var temp = "<td>" + common_terms.out_of_range + "</td>";
          }
        }
        html += temp;
      }
    }
    html += "</tr>";
  }
    return html;
}

function renderTables(products, temperatureRange, viscosityRange, compareIds) {
// console.log(compareIds)
  
  $("#tc").html("");
  $("#hc").html("");
  $("#kv").html("");
  $("#den").html("");
  $("#vp").html("");

  for( var x=1; x<=4; x++ ) {
    // Product Dropdown
    $("#name-" + x).text(common_terms.select_fluid);
    $("#name-" + x).data("product", "0");
    $("#name-floater-" + x).text("");
    //temps
    $("#maxbulk-" + x).html("");
    $("#maxfilm-" + x).html("");
    $("#pourpoint-" + x).html("");
    // safety data
    $("#flashpoint-" + x).html("");
    $("#firepoint-" + x).html("");
    $("#autoignition-" + x).html("");
    // Thermal Properties
    $("#thermalexpansion-" + x).html("");
    // Physical properties
    $("#chemical-composition-" + x).html("");
    $("#appearance-" + x).html("");
    $("#dist10-" + x).html("");
    $("#dist90-" + x).html("");
    $("#amw-" + x).html("");
    $("#sg-" + x).html("");
  }
  // Thermal Conductivity
  generateTables( "tc", "thermal_conductivity", products, temperatureRange );

  //Heat Capacity
  generateTables( "hc", "heat_capacity", products, temperatureRange );

  //Kinematic Viscosity
  generateTables( "kv", "kinematic_viscosity", products, viscosityRange );

  //Density
  generateTables( "den", "density", products, temperatureRange );

  //Density
  generateTables( "vp", "vapour_pressure", products, temperatureRange );


// console.log(products)
  for( var i=0; i<products.length; i++) {
    var x = i + 1;
    //reset values


    var max_temp = passTemp(products[i].max_temp);
    var film_temp = passTemp(products[i].film_temp);
    var pour_point = passTemp(products[i].pour_point)
    var flashpoint = passTemp(products[i].flashpoint)
    var fire_point = passTemp(products[i].fire_point)
    var auto_ignition_temp = passTemp(products[i].auto_ignition_temp)
    var thermal_expansion_coefficient = passTec(products[i].thermal_expansion_coefficient);
    var chemical_composition = products[i].chemical_composition;
    var distillation_range_10 = passTemp(products[i].distillation_range_10);
    var distillation_range_90 = passTemp(products[i].distillation_range_90);
    var average_molecular_weight = products[i].average_molecular_weight;
    var specific_gravity = products[i].specific_gravity != "N/A" ? roundNumber(products[i].specific_gravity, 3) : "N/A";
    if( products[i].duratherm_product == "yes" ) {
      var appearance = commonTerms.appearance_dura;
    } else {
      var appearance ="";
    }

    // Product Dropdown
    $("#name-" + x).text(products[i].name);
    $("#name-" + x).data("product", products[i].id);
    $("#name-floater-" + x).text(products[i].name);
    //temps
    $("#maxbulk-" + x).html(max_temp);
    $("#maxfilm-" + x).html(film_temp);
    $("#pourpoint-" + x).html(pour_point);
    // safety data
    $("#flashpoint-" + x).html(flashpoint);
    $("#firepoint-" + x).html(fire_point);
    $("#autoignition-" + x).html(auto_ignition_temp);
    // Thermal Properties
    $("#thermalexpansion-" + x).html(thermal_expansion_coefficient);
    // Physical properties
    $("#chemical-composition-" + x).html(chemical_composition);
    $("#appearance-" + x).html(appearance);
    $("#dist10-" + x).html(distillation_range_10);
    $("#dist90-" + x).html(distillation_range_90);
    $("#amw-" + x).html(average_molecular_weight);
    $("#sg-" + x).html(specific_gravity);
  }

}

function cnvTemp( temp ) {
  if( temp == "N/A" ) return temp;
  if( isMetric ) {
    var result = (temp-32) * 5/9;
    return parseInt(result + (result < 0 ? -.5 : .5)) + "&deg;C";
  }
  return temp + "&deg;F";
}

function passTemp( temp ){
  if( temp == "N/A" ) return temp;
  return temp  + "&deg;C";
}

function passTec ( tec ) {
  if( tec == "N/A" ) return tec;
  return roundNumber( tec * 1.8, 4 ) + "&#37;/&deg;C";
}

function getTempRanges(products) {  
  var tempRange = []; 
  var visRange = [];
  for( var i=0; i< products.length; i++ ) {
    if( ! _.isEmpty(products[i]) ) {
      var prodRange = products[i].temperature_range.split(",");
      for( var x=0; x< prodRange.length; x++ ) {
        tempRange.push( parseInt(prodRange[x]) );
      }
    }
  }

  for( var i=0; i< products.length; i++ ) {
    if( ! _.isEmpty(products[i]) ) {
      var prodRange = products[i].viscosity_range.split(",");
      for( var x=0; x< prodRange.length; x++ ) {
        visRange.push( parseInt(prodRange[x]) );
      }
    }
  }
  var temperatureRange = [];
  temperatureRange['reg'] = sort_unique(tempRange);
  temperatureRange['vis'] = sort_unique(visRange);
  return temperatureRange;
}

function sort_unique(arr) {
    if (arr.length === 0) return arr;
    arr = arr.sort(function (a, b) { return a*1 - b*1; });
    var ret = [arr[0]];
    for (var i = 1; i < arr.length; i++) { // start loop at 1 as element 0 can never be a duplicate
        if (arr[i-1] !== arr[i]) {
            ret.push(arr[i]);
        }
    }
    return ret;
}

function roundNumber(num, dec) {
  if( num === "N/A" ) return num;
  return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
}

function getProductsWithRange( products1, temperatureRange ) {  return products1.map(function( product ) {
    product.fluid_data = _.filter(product.fluid_data, function(fluid_data_item) {
       return _.includes(temperatureRange, fluid_data_item.temperature);
    });
    return product;
  });
}

function resolveProductProperties(products) {
  // loop through the products and create a temperature value for each degree
  // console.log(products);
    for(var i=0; i<products.length; i++) {
    if( _.isEmpty(products[i]) ) {
      continue;
    }
    if( products[i].duratherm_product !== "yes") {
      continue;
    }

      var temperatureValues = [];
      
      for(var j=0; j<products[i].fluid_data.length-1; j++) {
          
        var current = products[i].fluid_data[j];
        var next = products[i].fluid_data[j+1];
        //// console.log(current,next);
        for(var k=parseInt(current.temperature); k<parseInt(next.temperature); k++) {

          var pct = (k-parseInt(current.temperature))/(parseInt(next.temperature) - parseInt(current.temperature));
          //// console.log(k,pct,parseFloat(current.density)+((parseFloat(next.density)-parseFloat(current.density))*pct));
          temperatureValues.push({
            temperature:k,
            density:                parseFloat(current.density)+((parseFloat(next.density)-parseFloat(current.density))*pct),
            kinematic_viscosity:    parseFloat(current.kinematic_viscosity)+((parseFloat(next.kinematic_viscosity)-parseFloat(current.kinematic_viscosity))*pct),
            dynamic_viscosity:      parseFloat(current.dynamic_viscosity)+((parseFloat(next.dynamic_viscosity)-parseFloat(current.dynamic_viscosity))*pct),
            thermal_conductivity:   parseFloat(current.thermal_conductivity)+((parseFloat(next.thermal_conductivity)-parseFloat(current.thermal_conductivity))*pct),
            heat_capacity:          parseFloat(current.heat_capacity)+((parseFloat(next.heat_capacity)-parseFloat(current.heat_capacity))*pct),
            vapour_pressure:        parseFloat(current.vapour_pressure)+((parseFloat(next.vapour_pressure)-parseFloat(current.vapour_pressure))*pct)
          });
        }
      }
      temperatureValues.push(products[i].fluid_data[j]);
      products[i].fluid_data = temperatureValues;
        
    }
    // console.log(products);
    return products;
        // console.log(this.products);

}

// Fix column labels

var $parent = $(".compare-wrapper");
var $el = $(".compare-floater");
$parent.scroll(function() {
  if( $parent.scrollTop() > 50 )  {
    // $(".fluids").slideUp();
    $el.fadeIn().css('display', 'flex');
  } else {
    $el.fadeOut();
  }
  $el.css('top', $parent.scrollTop());
});

function generateTables( el, param, products, temperatureRange ) {
  var html = "";
  var el = $("#" + el);

  for( var i=0; i<temperatureRange.length; i++ ) {
    var rowClass = i % 2 ? " grey-row": "";
    html += "<div class='data-row" + rowClass + "'>";
    html += "<div class='table-label'>" + passTemp(temperatureRange[i]) + "</div>";
    for( var j=0; j<=3; j++ ) {
      if( ( _.isEmpty(products[j]))  ) {
        html += "<div class='table-data'><span> </span></div>";
      } else {
        var currentTemp = parseInt(temperatureRange[i]);
        for( var x=0; x< products[j].fluid_data.length; x++ ){
          var productTemp = parseInt(products[j].fluid_data[x].temperature);
          if( currentTemp == productTemp ){
            var temp = "<div class='table-data'><span>" + roundNumber(products[j].fluid_data[x][param], 3) +"</span></div>";
            break;
          } else if( products[j].duratherm_product !== "yes") {
            var temp = "<div class='table-data'><span>N/A</span></div>";
          } else {
            var temp = "<div class='table-data'><span>" + common_terms.out_of_range + "</span></div>";
          }
        }
        html += temp;
      }
    }
    html += "</div>";
  }
  el.html(html);
}

jQuery.fn.tableToCSV = function() {
    
    var clean_text = function(text){
        text = text.replace(/"/g, '""');
        text = text.replace("Â","");
        return '"'+text+'"';
    };
    
  $(this).each(function(){
      var table = $(this);
      var caption = "FluidComparisonTable";
      var title = [];
      var rows = [];

      $(this).find('tr').each(function(){
        var data = [];
        $(this).find('th').each(function(){
          var text = clean_text($(this).text());
          title.push(text);
          });
        $(this).find('td').each(function(){
          var text = clean_text($(this).text());
          data.push(text);
          });
        data = data.join(",");
        rows.push(data);
        });
      title = title.join(",");
      rows = rows.join("\n");

      var csv = title + rows;
      var textEncoder = new CustomTextEncoder('windows-1252', {NONSTANDARD_allowLegacyEncoding: true});
      var csvContentEncoded = textEncoder.encode([csv]);
      var blob = new Blob([csvContentEncoded], {type: 'text/csv;charset=windows-1252;'});
      saveAs(blob, 'Fluid_Comparison_Table.csv');
  });
    
};